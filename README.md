This is just a fork of the 'Open in WhatsApp' app by [@ShubhamTyagi](https://gitlab.com/ShubhamTyagi) to continue its development. Well, to try and perform some minimal maintenance at least. Unfortunately, I can't really promise I'll maintain it in the long run.

The original [GitHub repo](https://github.com/SubhamTyagi/openinwa) has been archived, so I haven't able to open an issue to ask if this is okay. I hope it is! Note: I must admit I haven't tried to send an email but I'll try to do it soon.

Personally, I'm just interested in a simple feature to open the contact list directly. Like this, I can avoid allowing WhatsApp access to my contacts but still be able to open a chat with an existing contact.

Eventually, I'd also like to have an option to immediately open WhatsApp after a contact has been selected. In fact, I would completely remove the "share" button and would open it directly in WhatsApp, but I'm sure there are some valid use cases for this (or it wouldn't be there).

I'll probably also fix https://github.com/SubhamTyagi/openinwa/issues/14, which seems relatively straight-forward to solve.

Thank you very much for your work, @ShubhamTyagi!

Here is the original README:

## Open in WhatsApp [![Build Status](https://travis-ci.org/SubhamTyagi/openinwa.svg?branch=master)](https://travis-ci.org/SubhamTyagi/openinwa)

 - This app uses WhatsApp public api 'click to chat' feature to open a chat with any number without saving to your phone book.
 - You can create an url link that will open WhatsApp on the specified number. This is a WhatsApp feature, you don't need this app to open the link, only to create it.

####  Download
[<img src="https://f-droid.org/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/io.github.subhamtyagi.openinwhatsapp/)

### Thanks

 - [ialokim](https://github.com/ialokim) for[ android-phone-number-field](https://github.com/ialokim/android-phone-field) which is license under Apache-2.0,

 ### License

 - [GPL V3](https://github.com/SubhamTyagi/openinwa/blob/master/LICENSE)

